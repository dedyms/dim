FROM --platform=$BUILDPLATFORM registry.gitlab.com/dedyms/node:14-dev AS build-web
ARG DEBIAN_FRONTEND=noninteractive
USER $CONTAINERUSER
RUN git clone https://github.com/Dusk-Labs/dim.git $HOME/dim
#RUN git clone https://github.com/martadinata666/dim.git $HOME/dim
WORKDIR $HOME/dim/ui
RUN yarn && yarn build


FROM --platform=$BUILDPLATFORM rust:slim-bullseye AS build-app
RUN apt update && \
    apt install -y ca-certificates libva2 libva-dev sqlite3 libdrm2 \
                   libdrm-dev libdrm-amdgpu1 curl \
                   build-essential pkg-config libssl-dev
COPY --from=build-web /home/debian/dim /dim
WORKDIR /dim
RUN sqlite3 -init ./database/migrations/*.sql ./dim_dev.db
RUN DATABASE_URL="sqlite:///dim/dim_dev.db" cargo build --release


FROM --platform=$BUILDPLATFORM registry.gitlab.com/dedyms/debian:vaapi
ENV RUST_BACKTRACE=full
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_DIR=/etc/ssl/certs
ENV DEBIAN_FRONTEND=noninteractive
RUN echo "deb http://mirror.poliwangi.ac.id/debian/ bullseye non-free" >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
            ca-certificates intel-media-va-driver-non-free \
            libsqlite3-dev libva2 libva-drm2 libharfbuzz0b \
            libfontconfig libfribidi0 libtheora0 libvorbis0a libvorbisenc2 && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /opt/dim/ && \
    chown $CONTAINERUSER:$CONTAINERUSER /opt/dim/
COPY --from=build-app --chown=$CONTAINERUSER:$CONTAINERUSER /dim/target/release/dim /opt/dim/dim
USER $CONTAINERUSER
RUN mkdir -p /opt/dim/config /opt/dim/utils
RUN ln -s /usr/lib/jellyfin-ffmpeg/ffmpeg /opt/dim/utils/ffmpeg && \
    ln -s /usr/lib/jellyfin-ffmpeg/ffprobe /opt/dim/utils/ffprobe
EXPOSE 8000
WORKDIR /opt/dim
VOLUME ["/opt/dim/config"]
CMD ["./dim"]

